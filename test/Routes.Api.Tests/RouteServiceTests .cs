﻿using AutoMapper;
using FakeItEasy;
using Routes.Api.Infrastructure.Exceptions;
using Routes.Api.Infrastructure.Models;
using Routes.Api.Infrastructure.Repositories;
using Routes.Api.Infrastructure.Services;
using System.Collections.Generic;
using Xunit;

namespace Routes.Api.Tests
{
    public class RouteServiceTests
    {
        protected RouteService ServiceUnderTests { get; }
        protected IRouteRepository RouteRepositoryFake { get; } 
        protected IHereApiClient HereApiClientFake { get; }
        protected IMapper Mapper { get; }

        public RouteServiceTests()
        {
            RouteRepositoryFake = A.Fake<IRouteRepository>();
            HereApiClientFake = A.Fake<IHereApiClient>();
            Mapper = A.Fake<IMapper>();
            ServiceUnderTests = new RouteService(RouteRepositoryFake, HereApiClientFake, Mapper);
        }

        public class ReadAll : RouteServiceTests
        {
            [Fact]
            public void ShouldReturn_AllRoutes()
            {
                // Arrange
                var expectedRoutes = new List<Route>
                {
                    new Route(1, new List<Point>())
                };
                A.CallTo(() => RouteRepositoryFake.ReadAll()).Returns(expectedRoutes);

                // Act
                var result = ServiceUnderTests.ReadAll();

                // Assert
                Assert.Same(expectedRoutes, result);
            }
        }

        public class ReadOne : RouteServiceTests
        {
            [Fact]
            public void ShouldReturn_TheExpectedRoute()
            {
                // Arrange
                var routeId = 1;
                var expectedRoute = new Route(routeId, new List<Point>());
                A.CallTo(() => RouteRepositoryFake.ReadOne(routeId)).Returns(expectedRoute);

                // Act
                var result = ServiceUnderTests.ReadOne(routeId);

                // Assert
                Assert.Same(expectedRoute, result);
            }

            [Fact]
            public void ShouldThrowRouteNotFoundException_WhenRouteDoesNotExist()
            {
                // Arrange
                var routeId = 1;
                var expectedRoute = new Route(routeId, new List<Point>());
                A.CallTo(() => RouteRepositoryFake.ReadOne(routeId)).Returns(default(Route));

                // Act & Assert
                Assert.Throws<RouteNotFoundException>(() => ServiceUnderTests.ReadOne(routeId));
            }
        }
    }
}
