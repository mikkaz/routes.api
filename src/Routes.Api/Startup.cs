﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Routes.Api.Infrastructure.Extensions;
using Routes.Api.Infrastructure.Services;
using Routes.Api.Infrastructure.Settings;

namespace Routes.Api.Infrastructure
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddCustomSwaggerGen();
            services.AddDependencyInjectionConfiguration();
            services.Configure<HereApiSettings>(Configuration.GetSection("HereApiSettings"));
            services.AddHttpClient<IHereApiClient, HereApiClient>();
            services.AddAutoMapper(typeof(MappingProfile));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseCustomSwaggerUI();
            app.UseMvc();
        }
    }
}
