﻿using System.Collections.Generic;

namespace Routes.Api.Infrastructure.Models
{
    public class MatchedRoute
    {
        public List<MatchedPoint> TracePoints{ get; set; }
    }
}
