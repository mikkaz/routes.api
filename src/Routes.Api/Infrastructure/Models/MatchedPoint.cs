﻿using Newtonsoft.Json;

namespace Routes.Api.Infrastructure.Models
{
    public class MatchedPoint
    {
        [JsonProperty(PropertyName = "lat")]
        public double Latitude { get; set; }

        [JsonProperty(PropertyName = "lon")]
        public double Longitude { get; set; }

        [JsonProperty(PropertyName = "latMatched")]
        public double LatitudeMatched { get; set; }

        [JsonProperty(PropertyName = "lonMatched")]
        public double LongitudeMatched { get; set; }
    }
}