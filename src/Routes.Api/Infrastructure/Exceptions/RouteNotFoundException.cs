﻿namespace Routes.Api.Infrastructure.Exceptions
{
    public class RouteNotFoundException : RoutesApiException
    {
        public RouteNotFoundException()
        {

        }

        public RouteNotFoundException(string message) : base(message)
        {

        }
    }
}
