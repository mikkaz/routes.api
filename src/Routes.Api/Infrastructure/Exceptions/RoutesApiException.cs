﻿using System;
using System.Runtime.Serialization;

namespace Routes.Api.Infrastructure.Exceptions
{
    public class RoutesApiException : Exception
    {
        public RoutesApiException()
        {
        }

        public RoutesApiException(string message) : base(message)
        {
        }

        public RoutesApiException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected RoutesApiException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
