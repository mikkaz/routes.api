﻿using AutoMapper;
using Routes.Api.Infrastructure.Models;

namespace Routes.Api.Infrastructure
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            CreateMap<MatchedPoint, Point>()
                .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.LatitudeMatched))
                .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.LongitudeMatched));

            CreateMap<MatchedRoute, Route>()
                .ForMember(dest => dest.Points, opt => opt.MapFrom(src => src.TracePoints));
        }
    }
}
