﻿using Microsoft.AspNetCore.Builder;

namespace Routes.Api.Infrastructure.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseCustomSwaggerUI(this IApplicationBuilder app)
        {
            app.UseSwaggerUI(setupAction =>
            {
                setupAction.SwaggerEndpoint(
                    "/swagger/PointsAPISpecification/swagger.json",
                    "Points API");
                setupAction.RoutePrefix = "";
            });
            return app;
        }
    }
}
