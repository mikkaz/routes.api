﻿using Microsoft.Extensions.DependencyInjection;
using Routes.Api.Infrastructure.Repositories;
using Routes.Api.Infrastructure.RouteConverter;
using Routes.Api.Infrastructure.Serialization;
using Routes.Api.Infrastructure.Services;
using System;
using System.IO;
using System.IO.Abstractions;
using System.Reflection;

namespace Routes.Api.Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCustomSwaggerGen(this IServiceCollection services)
        {
            services.AddSwaggerGen(setupAction =>
            {
                setupAction.SwaggerDoc("PointsAPISpecification", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "Points API",
                    Version = "1",
                    Contact = new Swashbuckle.AspNetCore.Swagger.Contact()
                    {
                        Email = "kazimierczakmikolaj@gmail.com",
                        Name = "Mikołaj Kazimierczak",
                        Url = "https://bitbucket.org/mikkaz"
                    }
                });
                var xmlCommentsFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlCommentFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentsFile);
                setupAction.IncludeXmlComments(xmlCommentFullPath);
            });
            return services;
        }
        public static IServiceCollection AddDependencyInjectionConfiguration(this IServiceCollection services)
        {
            services.AddScoped<IRouteRepository, JsonRouteRepository>();
            services.AddScoped<IRouteService, RouteService>();
            services.AddScoped<IFileSystem, FileSystem>();
            services.AddScoped<IJsonSerializer, NewtonsoftJsonSerializer>();
            services.AddScoped<IRouteConverter, RouteToGPXConverter>();

            return services;
        }
    }
}
