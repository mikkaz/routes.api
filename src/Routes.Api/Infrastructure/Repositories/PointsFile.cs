﻿namespace Routes.Api.Infrastructure.Repositories
{
    public class PointsFile
    {
        public int FileId { get; set; }
        public string Content { get; set; }
    }
}