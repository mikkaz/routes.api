﻿using System.IO;
using System.Text;

namespace Routes.Api.Infrastructure.RouteConverter
{
    public class StringWriterWithoutEncoding : StringWriter
    {
        public StringWriterWithoutEncoding() : base()
        {
        }

        public override Encoding Encoding
        {
            get { return null; }
        }
    }
}
