﻿using Routes.Api.Infrastructure.Models;
using System.Linq;
using System.Xml.Linq;

namespace Routes.Api.Infrastructure.RouteConverter
{
    public class RouteToGPXConverter : IRouteConverter
    {
        public string Convert(Route route)
        {
            XDocument document = new XDocument();
            document.Declaration = new XDeclaration("1.0", null, null);

            XNamespace xNamespace = "http://www.topografix.com/GPX/1/0";
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            XElement gpxBase =
                new XElement(xNamespace + "gpx",
                    new XAttribute("version", "1.0"),
                    new XAttribute(XNamespace.Xmlns + "xsi", "http://www.w3.org/2001/XMLSchema-instance"),
                    new XAttribute(xsi + "schemaLocation", "http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd")
                    );
            XElement gpxRoute =
                new XElement("trk",
                    new XElement("trkseg",
                        route.Points.Select(point =>
                            new XElement("trkpt",
                                new XAttribute("lat", point.Latitude),
                                new XAttribute("lon", point.Longitude)))));
            gpxBase.Add(gpxRoute);
            document.Add(gpxBase);

            using (var stringWriter = new StringWriterWithoutEncoding())
            {
                document.Save(stringWriter);
                return stringWriter.ToString();
            }
        }
    }
}
