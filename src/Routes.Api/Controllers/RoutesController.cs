﻿using Microsoft.AspNetCore.Mvc;
using Routes.Api.Infrastructure.Exceptions;
using Routes.Api.Infrastructure.Models;
using Routes.Api.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Routes.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoutesController : ControllerBase
    {
        private readonly IRouteService _routeService;

        public RoutesController(IRouteService routeService)
        {
            _routeService = routeService ?? throw new ArgumentNullException(nameof(routeService));
        }

        /// <summary>
        /// Read all routes
        /// </summary>
        /// <param name="routeMatching">Indicates whether route matching should be enabled or disabled</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<Route>), 200)]
        public async Task<IActionResult> ReadAll(bool routeMatching = false)
        {
            if (routeMatching)
                return Ok(await _routeService.ReadAllWithRouteMatching());
            else
                return Ok(_routeService.ReadAll());
        }

        /// <summary>
        /// Read route with specific Id
        /// </summary>
        /// <param name="id">The id of the route</param>
        /// <param name="routeMatching">Indicates whether route matching should be enabled or disabled</param>
        /// <returns></returns>
        /// <response code="404">A route with specified Id was not found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Route), 200)]
        [ProducesResponseType(typeof(string), 404)]
        public async Task<IActionResult> ReadOne(int id, bool routeMatching = false)
        {
            try
            {
                if (routeMatching)
                    return Ok(await _routeService.ReadOneWithRouteMatching(id));
                else
                    return Ok(_routeService.ReadOne(id));
            }
            catch (RouteNotFoundException exception)
            {
                return NotFound(exception.Message);
            }
        }
    }
}